package com.mygdx.game;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;
import com.google.example.games.basegameutils.BaseGameUtils;
import com.google.example.games.basegameutils.GameHelper;
import com.mygdx.interstellarmayhem.InterstellarMayhem;
import com.mygdx.interstellarmayhem.PlayServices;

public class AndroidLauncher extends AndroidApplication implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, PlayServices {
	GoogleApiClient myClient;
	private static int RC_SIGN_IN = 9001;
	private boolean mResolvingConnectionFailure = false;
	private boolean mAutoStartSignInflow = true;
	private boolean mSignInClicked = false;
	private GameHelper gameHelper;
	private final static int requestCode = 1;
	private Toast toast1;
	private Toast toast2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (checkPlayServices()){
			myClient = new GoogleApiClient.Builder(this)
					.addApi(Plus.API)
					.addScope(Plus.SCOPE_PLUS_PROFILE)
					.addConnectionCallbacks(this)
					.addOnConnectionFailedListener(this)
					.build();
		}
		gameHelper = new GameHelper(this, GameHelper.CLIENT_GAMES);
		gameHelper.enableDebugLog(false);
		toast1 = Toast.makeText(getApplicationContext(), "Swipe Left Or Right To Dodge Asteroids And Enemy Craft!", Toast.LENGTH_SHORT);
		toast2 = Toast.makeText(getApplicationContext(), "Collect Boson Pulses To Score Points And Unlock Achievements!", Toast.LENGTH_SHORT);

		GameHelper.GameHelperListener gameHelperListener = new GameHelper.GameHelperListener()
		{
			@Override
			public void onSignInFailed(){ }

			@Override
			public void onSignInSucceeded(){ }
		};

		gameHelper.setup(gameHelperListener);
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		initialize(new InterstellarMayhem(this), config);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		gameHelper.onActivityResult(requestCode, resultCode, data);
	}

	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (resultCode == -1) {
			return false;
		}
		else {
			return true;
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		gameHelper.onStart(this);
	}

	protected void onStop() {
		super.onStop();
		gameHelper.onStop();
	}

	@Override
	public void onConnected(Bundle bundle) {
	}

	@Override
	public void onConnectionSuspended(int i) {
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		if (mResolvingConnectionFailure) {
			// Already resolving
			return;
		}
		// If the sign in button was clicked or if auto sign-in is enabled,
		// launch the sign-in flow
		if (mSignInClicked || mAutoStartSignInflow) {
			mAutoStartSignInflow = false;
			mSignInClicked = false;
			mResolvingConnectionFailure = true;
			// Attempt to resolve the connection failure using BaseGameUtils.
			if (!BaseGameUtils.resolveConnectionFailure(this,
					myClient, connectionResult,
					RC_SIGN_IN, getString(R.string.signin_other_error))) {
				mResolvingConnectionFailure = false;
			}
		}
	}

	@Override
	public void signIn()
	{
		try
		{
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					gameHelper.beginUserInitiatedSignIn();
				}
			});
		}
		catch (Exception e)
		{
			Gdx.app.log("MainActivity", "Log in failed: " + e.getMessage() + ".");
		}
	}


	@Override
	public void submitScore(int highScore)
	{
		if (isSignedIn() == true)
		{
			Games.Leaderboards.submitScore(gameHelper.getApiClient(),
					getString(R.string.leaderboard_interstellar_mayhem_hall_of_fame), highScore);
		}
	}


	@Override
	public void showScore()
	{
		if (isSignedIn() == true)
		{
			startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(),
					getString(R.string.leaderboard_interstellar_mayhem_hall_of_fame)), requestCode);
		}
		else
		{
			signIn();
		}
	}

	@Override
	public boolean isSignedIn()
	{
		return gameHelper.isSignedIn();
	}

	public void unlockAchievement1(){
		if (isSignedIn() == true)
		{
		Games.Achievements.unlock(gameHelper.getApiClient(),
				getString(R.string.achievement_collect_a_boson));
		}
	}

	public void unlockAchievement2(){
		if (isSignedIn() == true)
		{
		Games.Achievements.unlock(gameHelper.getApiClient(),
				getString(R.string.achievement_collect_10_bosons));
		}
	}

	public void unlockAchievement3(){
		if (isSignedIn() == true)
		{
		Games.Achievements.unlock(gameHelper.getApiClient(),
				getString(R.string.achievement_collect_100_boson));
		}
	}

	public void unlockAchievement4(){
		if (isSignedIn() == true) {
			Games.Achievements.unlock(gameHelper.getApiClient(),
					getString(R.string.achievement_strong_flier));
		}
	}


	public void showMsg1(){
		handler.post(new Runnable()
		{
			@Override
			public void run() {
				toast1.show();
			}
		});
	}

	public void stopMsg1() {
		{
			handler.post(new Runnable() {
				@Override
				public void run() {
					toast1.cancel();
				}
			});
		}
	}

	public void showMsg2(){
		handler.post(new Runnable()
		{
			@Override
			public void run() {
				toast2.show();
			}
		});
	}

	public void stopMsg2() {
		{
			handler.post(new Runnable() {
				@Override
				public void run() {
					toast2.cancel();
				}
			});
		}
	}
}

