package com.mygdx.game.desktop;


import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.interstellarmayhem.InterstellarMayhem;
import com.mygdx.interstellarmayhem.PlayServices;


public class DesktopLauncher {
	static PlayServices playServices;
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = InterstellarMayhem.WIDTH;
		config.height = InterstellarMayhem.HEIGHT;
		new LwjglApplication(new InterstellarMayhem(playServices), config);
	}
}
