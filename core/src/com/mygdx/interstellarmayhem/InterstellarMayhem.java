package com.mygdx.interstellarmayhem;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.interstellarmayhem.States.MenuState;

/**
 * Created by Beau Johnson (TehBeast) on 25-Aug-16.
 */

public class InterstellarMayhem extends ApplicationAdapter {

	private com.mygdx.interstellarmayhem.States.GameStateManager gsm;
	private SpriteBatch sb;
	public static final int WIDTH = 480;
	public static final int HEIGHT = 800;
	public static final int SCREEN_RIGHT_BOUNDS = WIDTH - 128;
	public static final int SCREEN_LEFT_BOUNDS = WIDTH - 480;
	public static final float SCREEN_DELAY = .66f;
	public static final float DEATH_DELAY = .50f;
	public static PlayServices playServices;

	public InterstellarMayhem(PlayServices playServices){
			this.playServices = playServices;
		}

	@Override
	public void create () {
		sb = new SpriteBatch();
		gsm = new com.mygdx.interstellarmayhem.States.GameStateManager();
		Gdx.gl.glClearColor(0,0,0,1);
		gsm.push(new MenuState(gsm, playServices));
	}


	@Override
	public void render () {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.render(sb);
	}


	@Override
	public void dispose () {
		sb.dispose();
	}
}

