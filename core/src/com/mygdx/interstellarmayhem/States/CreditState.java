package com.mygdx.interstellarmayhem.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.interstellarmayhem.PlayServices;

/**
 * Created by Beau Johnson (tehBeast) on 24-Sep-16.
 */

public class CreditState extends State {
    Sprite bg, mainMenu;
    private Music bgMusic;
    Sound btnClick;
    com.mygdx.interstellarmayhem.Credits credits = new com.mygdx.interstellarmayhem.Credits();
    PlayServices playServices;


    public CreditState(GameStateManager gsm, PlayServices playServices) {
        super(gsm);
        this.playServices = playServices;
        bg = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/GalaxyBG.bmp")));
        mainMenu = new Sprite(new Texture(Gdx.files.internal("highScoreAssets/menu_button_blue.png")));
        btnClick = Gdx.audio.newSound(Gdx.files.internal("mainMenuAssets/ButtonClick.mp3"));
        bgMusic = Gdx.audio.newMusic(Gdx.files.internal("mainMenuAssets/ObservingTheStar.ogg"));
        bgMusic.setLooping(true);
        bgMusic.play();
        cam.setToOrtho(false, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH, com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT);
        super.mouse = new Vector3();
    }


    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            mouse.set(Gdx.input.getX(), Gdx.input.getY(), 0); //when the screen is touched, the coordinates are inserted into the vector
            System.out.println("mouse click: " + Gdx.input.getX()+ " " + Gdx.input.getY());
            cam.unproject(mouse); // calibrates the input to  camera dimension
            if (mouse.x < (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 + (mainMenu.getWidth() / 2) - 48)
                    && mouse.x > (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 - (mainMenu.getWidth() / 2)) + 48) {
                if (mouse.y > (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 575) - (mainMenu.getHeight() / 2)
                        && mouse.y < (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 475) - (mainMenu.getHeight() / 2)) {
                    mainMenu = new Sprite(new Texture(Gdx.files.internal("highScoreAssets/MenuBtnRed.png")));
                    System.out.println("Menu Button Clicked");
                    btnClick.play();
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            gsm.set(new com.mygdx.interstellarmayhem.States.MenuState(gsm, playServices));
                        }
                    }, com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_DELAY);
                }
            }
        }
    }


    @Override
    public void update(float dt) {
        handleInput();
    }


    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(bg, 0,0);
        sb.draw(mainMenu, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH/2 - (mainMenu.getWidth()/2), (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 400) - (mainMenu.getHeight() * 1.5F));
        sb.end();
        credits.stage.draw();
    }


    @Override
    public void dispose() {
        bgMusic.dispose();
        btnClick.dispose();
    }
}
