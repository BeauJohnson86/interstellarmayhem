package com.mygdx.interstellarmayhem.States;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.interstellarmayhem.Hud;
import com.mygdx.interstellarmayhem.InterstellarMayhem;
import com.mygdx.interstellarmayhem.PlayServices;

/**
 * Created by Beau Johnson (tehBeast) on 28-Aug-16.
 */

public class GameState extends State implements InputProcessor{

    //Setup variables for the Game State
    private static final int ASTEROID_COUNT              = 3;
    private static final int BOSON_COUNT                 = 3;
    private static final int ENEMY_COUNT                 = 1;
    private static final int SPACING                     = 128;
    private double           tempNum;
    private Float            gap;
    private int              livesRemaining              = 0;
    private int              score                       = 0;
    int                      sourceY                     = 0;
    Hud                      hud = new Hud(); //Hud object to display the score, lives and time
    private Music            bgMusic;
    private Sound            playerCrash, collectBoson;
    int                      xPosition;
    PlayServices             playServices;
    float                    timePassed;
    TextureAtlas             enemyTA = new TextureAtlas(Gdx.files.internal("enemyAssets/redEnemyShip.atlas")); // enemy moving sprite
    Texture                  texture = new Texture(Gdx.files.internal("mainMenuAssets/GalaxyBG.bmp")); // background image
    Vector2                  lastTouch = new Vector2();

    com.mygdx.interstellarmayhem.EnvironmentObjects.Player player; // player object
    private Array<com.mygdx.interstellarmayhem.EnvironmentObjects.Enemy> enemies;
    private Array<com.mygdx.interstellarmayhem.EnvironmentObjects.Asteroid> asteroids;
    private Array<com.mygdx.interstellarmayhem.EnvironmentObjects.Boson> bosons;

    //GameState constructor takes in a GameStateManager and PlayServices parameter
    public GameState(GameStateManager gsm, final PlayServices playServices){
        //Initialise variables and assets for the GameState
        super(gsm);
        this.playServices = playServices;
        Gdx.app.setLogLevel(Application.LOG_INFO);
        cam.setToOrtho(false, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH, com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT);
        texture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        player  = new com.mygdx.interstellarmayhem.EnvironmentObjects.Player("DEFAULT", 3, true, true);
        bgMusic = Gdx.audio.newMusic(Gdx.files.internal("gameStateAssets/inGameMusic.ogg")); // music track for game
        bgMusic.setLooping(true);
        bgMusic.play();
        livesRemaining = player.getLives();
        hud.updateLives(livesRemaining); // update the hud with initial lives value
        playerCrash = Gdx.audio.newSound(Gdx.files.internal("gameStateAssets/crash.ogg")); // sound for player impact
        collectBoson = Gdx.audio.newSound(Gdx.files.internal("gameStateAssets/bosonCollect.mp3")); // sound for collecting boson
        Gdx.input.setInputProcessor(this);
        player.setyPosition(125);
        xPosition = player.getxPosition();
        enemies = new Array<com.mygdx.interstellarmayhem.EnvironmentObjects.Enemy>(); //enemy Array
        asteroids = new Array<com.mygdx.interstellarmayhem.EnvironmentObjects.Asteroid>(); //asteroid Array
        bosons = new Array<com.mygdx.interstellarmayhem.EnvironmentObjects.Boson>(); //boson Array

        //loop to create Asteroids and add them to the Array
        for(int i = 0; i < ASTEROID_COUNT; i++){
            asteroids.add(new com.mygdx.interstellarmayhem.EnvironmentObjects.Asteroid(com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT * (i+1) + SPACING));
        }
        //loop to create Enemies and add them to the Array
        for(int i = 0; i < ENEMY_COUNT; i++){
            enemies.add(new com.mygdx.interstellarmayhem.EnvironmentObjects.Enemy("ENEMY" + i, 1, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH,
                    com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT, true, true, enemyTA));
        }
        //loop to create Bosons and add them to the Array
        for(int i = 0; i < BOSON_COUNT; i++){
            bosons.add(new com.mygdx.interstellarmayhem.EnvironmentObjects.Boson((com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT * (i+1) + SPACING )*1.33f));
        }
    }


    @Override
    public void handleInput() {
    }


    @Override
    public void update(float dt) {
        hud.update(dt);
        // Show 1st Toast Message to user (How to play instructions)
        if (Integer.parseInt(hud.getTime()) == 1){
            playServices.showMsg1();
        }
        // Manually stop 1st Toast Message Due to Toast being on another thread
        if (Integer.parseInt(hud.getTime()) == 5) {
            playServices.stopMsg1();
        }
        // Show 2nd Toast Message to user (How to play instructions)
        if (Integer.parseInt(hud.getTime()) == 6) {
            playServices.showMsg2();
        }
        // Manually stop 2nd Toast Message Due to Toast being on another thread
        if (Integer.parseInt(hud.getTime()) == 10){
            playServices.stopMsg2();
        }
        // Updates the asteroids position so that they move down the screen, once the
        // asteroid has left the screen it is repositioned to be used again.
        for (com.mygdx.interstellarmayhem.EnvironmentObjects.Asteroid asteroid : asteroids){
            asteroid.reposition(Math.round(asteroid.getAsteroidPosition().y - timePassed * .09));
            if (cam.position.y - (cam.viewportHeight) > (asteroid.getAsteroidPosition().y + 128)){
                asteroid.reposition(com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT + generateGap());
                asteroid.setAsteroidXPosition(asteroid.generateAsteroidXLocation());
            }
            // handle player collision with asteroid
            if(asteroid.collides(player.getBoundsPlayer())){
                playerCrash.play();
                livesRemaining--;
                hud.updateLives(livesRemaining);
                player.setLives(livesRemaining);
                //reposition the asteroid both X and Y positions so it appears as a different asteroid
                asteroid.reposition(com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT + generateGap());
                asteroid.setAsteroidXPosition(asteroid.generateAsteroidXLocation());
                player.playerDied();
                // handle if player has no lives remaining
                if (player.getLives() == 0){
                    bgMusic.stop();
                    //timer to allow for player death animation to play.
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            player.dispose();
                            bosons.clear();
                            enemies.clear();
                            asteroids.clear();
                            // set EndgGameState in GameStateManager to change state.
                            gsm.set(new EndGameState(gsm, String.valueOf(score), hud.getTime(), playServices));
                        }
                    }, com.mygdx.interstellarmayhem.InterstellarMayhem.DEATH_DELAY);
                }
            }
        }
        // Updates the enemy position so that they move down the screen, once the
        // enemy has left the screen it is repositioned to be used again.
        for (com.mygdx.interstellarmayhem.EnvironmentObjects.Enemy enemy : enemies){
            enemy.reposition(Math.round(enemy.getEnemyPosition().y - timePassed * .006));
            if (cam.position.y - (cam.viewportHeight) > (enemy.getEnemyPosition().y + 128)){
                enemy.setEnemyXPosition(enemy.generateEnemyXLocation());
                enemy.reposition(com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT + generateGap());
            }
            // handle player collision with enemy
            if(enemy.collides(player.getBoundsPlayer())) {
                playerCrash.play();
                livesRemaining--;
                hud.updateLives(livesRemaining);
                enemy.reposition(com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT + generateGap());
                enemy.setEnemyXPosition(enemy.generateEnemyXLocation());
                player.playerDied();
                player.setLives(livesRemaining);
                // handle if player has no lives remaining
                if (player.getLives() == 0) {
                    bgMusic.stop();
                    //timer to allow for player death animation to play.
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            player.dispose();
                            bosons.clear();
                            enemies.clear();
                            asteroids.clear();
                            // set EndgGameState in GameStateManager to change state.
                            gsm.set(new EndGameState(gsm, String.valueOf(score), hud.getTime(), playServices));
                        }
                    }, com.mygdx.interstellarmayhem.InterstellarMayhem.DEATH_DELAY);
                }
            }
        }
        // Updates the bosons position so that they move down the screen, once the
        // boson has left the screen, or been collected it is repositioned to be used again.
        for (com.mygdx.interstellarmayhem.EnvironmentObjects.Boson boson : bosons){
            boson.reposition(Math.round(boson.getBosonPosition().y - timePassed * .09));
            if (cam.position.y - (cam.viewportHeight) > (boson.getBosonPosition().y + 128)){
                boson.reposition(com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT + generateGap());
                boson.setBosonXPosition(boson.generateBosonXLocation());
            }
            // handle player collision with boson
            if(boson.collides(player.getBoundsPlayer())) {
                collectBoson.play();
                boson.reposition(com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT + generateGap());
                boson.setBosonXPosition(boson.generateBosonXLocation());
                score += 100;
                hud.addScore(score);
                // handle achievement 1
                if (score == 100){
                    playServices.unlockAchievement1();
                }
                // handle achievement 2
                if (score == 1000){
                    playServices.unlockAchievement2();
                }
                // handle achievement 3
                if (score == 10000){
                    playServices.unlockAchievement3();
                }
            }
        }
    }


    @Override
    public void render(SpriteBatch sb) {
        sourceY -= 1.5;
        timePassed += Gdx.graphics.getDeltaTime();
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(texture, 0, 0, 0, sourceY, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH, com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT);
        // Draws the asteroids onto the screen for each asteroid in the array
        for (com.mygdx.interstellarmayhem.EnvironmentObjects.Asteroid asteroid : asteroids) {
            sb.draw(asteroid.getAsteroid(), asteroid.getAsteroidPosition().x,
                    asteroid.getAsteroidPosition().y, 128, 128);
        }
        sb.end();
        hud.stage.draw();
        // Renders the enemies onto the screen for each asteroid in the array
        for (com.mygdx.interstellarmayhem.EnvironmentObjects.Enemy enemy : enemies) {
            enemy.render(sb);
        }
        // Renders the bosonss onto the screen for each asteroid in the array
        for (com.mygdx.interstellarmayhem.EnvironmentObjects.Boson boson : bosons){
            boson.render(sb);
        }
        // Renders the player
        player.render(sb);
    }


    @Override
    // Dispose of assets when this class is no longer required
    public void dispose() {
        player.dispose();
        bgMusic.dispose();
        enemies.clear();
        asteroids.clear();
        playerCrash.dispose();
        collectBoson.dispose();
    }

    public float generateGap(){
        tempNum = (Math.random() * (InterstellarMayhem.HEIGHT * 2));
        gap = new Float(tempNum);
        return gap;
    }


    @Override
    public boolean keyDown(int keycode) {
        return false;
    }


    @Override
    public boolean keyUp(int keycode) {
        return false;
    }


    @Override
    public boolean keyTyped(char character) {
        return false;
    }


    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        lastTouch.set(screenX,screenY);
        return true;
    }


    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return true;
    }


    @Override
    // handles the dragging of a players finger from left to right
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        Vector2 newTouch = new Vector2(screenX, screenY);
        float distanceMovedHorizontal = (newTouch.x - lastTouch.x) /13;
        // Determine if swipe to right and handle player movement
        if(newTouch.x > lastTouch.x && player.getxPosition() <= com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH) {
            // handle movement within screen bounds
            if (player.getxPosition() + Math.round(distanceMovedHorizontal) <= com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_RIGHT_BOUNDS) {
                player.setxPosition(player.getxPosition() + Math.round(distanceMovedHorizontal));
                player.getBoundsPlayer().setPosition(player.getxPosition(), player.getyPosition());
            }
            // handle movement that exceeds screen bounds
            else {
                player.setxPosition(com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_RIGHT_BOUNDS);
                player.getBoundsPlayer().setPosition(player.getxPosition(), player.getyPosition());
            }
        }
        // Determine if swipe to left and handle player movement
        if (newTouch.x < lastTouch.x && player.getxPosition() >= com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH - 480) {
            // handle movement within screen bounds
            if (player.getxPosition() + Math.round(distanceMovedHorizontal) >= com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_LEFT_BOUNDS) {
                player.setxPosition(player.getxPosition() + Math.round(distanceMovedHorizontal));
                player.getBoundsPlayer().setPosition(player.getxPosition(), player.getyPosition());
            }
            // handle movement that exceeds screen bounds
            else {
                player.setxPosition(com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_LEFT_BOUNDS);
                player.getBoundsPlayer().setPosition(player.getxPosition(), player.getyPosition());
            }
        }
            return true;
        }


    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }


    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
