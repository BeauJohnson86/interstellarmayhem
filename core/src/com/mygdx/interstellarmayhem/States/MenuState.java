package com.mygdx.interstellarmayhem.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.interstellarmayhem.MenuTitle;
import com.mygdx.interstellarmayhem.PlayServices;

/**
 * Created by Beau Johnson (TehBeast) on 25-Aug-16.
 */

public class MenuState extends State {

    private Sprite bg, startGame, credits, highScore;
    private Music bgMusic;
    private Sound btnClick;
    com.mygdx.interstellarmayhem.EnvironmentObjects.Player player;
    MenuTitle title = new MenuTitle();
    public static PlayServices playServices;


    public MenuState(GameStateManager gsm, PlayServices playServices){
        super(gsm);
        this.playServices = playServices;
        bg = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/GalaxyBG.bmp")));
        startGame = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/play_button_blue.png")));
        highScore = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/highscore_button_blue.png")));
        credits = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/credits_button_blue.png")));
        btnClick = Gdx.audio.newSound(Gdx.files.internal("mainMenuAssets/ButtonClick.mp3"));
        bgMusic = Gdx.audio.newMusic(Gdx.files.internal("mainMenuAssets/ObservingTheStar.ogg"));
        bgMusic.setLooping(true);
        bgMusic.play();
        player = new com.mygdx.interstellarmayhem.EnvironmentObjects.Player("DEFAULT", 3, true, true);
        cam.setToOrtho(false, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH, com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT);
        super.mouse = new Vector3();
    }


    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            mouse.set(Gdx.input.getX(), Gdx.input.getY(), 0); //when the screen is touched, the coordinates are inserted into the vector
            cam.unproject(mouse); // calibrates the input to  camera dimension

            // handles the events if the play button is pressed
            if (mouse.x < (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 + (startGame.getWidth() / 2) - 48)
                    && mouse.x > (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 - (startGame.getWidth() / 2) + 48)) {
                if (mouse.y > (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 275) - (startGame.getHeight() / 2)
                        && mouse.y < (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 425) + (startGame.getHeight() / 2)) {
                    startGame = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/play_button_pressed.png")));
                    btnClick.play();
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            gsm.set(new GameState(gsm, playServices));
                        }
                    }, com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_DELAY);
                }
            }
            // handles the events if the high score button is pressed
            if (mouse.x < (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 + (highScore.getWidth() / 2) - 48)
                    && mouse.x > (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 - (highScore.getWidth() / 2)) + 48) {
                if (mouse.y > (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 425) - (highScore.getHeight() / 2)
                        && mouse.y < (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 325) - (highScore.getHeight() / 2)) {
                    btnClick.play();
                    if (playServices.isSignedIn()) {
                        highScore = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/highscore_button_pressed.png")));
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                playServices.showScore();
                                highScore = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/highscore_button_blue.png")));
                            }
                        }, com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_DELAY);
                    }
                }
            }
            // handles the events if the credit button is pressed
            if (mouse.x < (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 + (credits.getWidth() / 2) - 48)
                    && mouse.x > (com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 - (credits.getWidth() / 2)) + 48) {
                if (mouse.y > (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 575) - (credits.getHeight() / 2)
                        && mouse.y < (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 475) - (credits.getHeight() / 2)) {
                    credits = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/credits_button_pressed.png")));
                    btnClick.play();
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            gsm.set(new CreditState(gsm, playServices));
                        }
                    }, com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_DELAY);
                }
            }
        }
    }


    @Override
    public void update(float dt) {
        handleInput();
    }


    @Override
    public void render(SpriteBatch sb) {

            sb.setProjectionMatrix(cam.combined);
            sb.begin();
            sb.draw(bg, 0, 0);
            sb.draw(startGame, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 - (startGame.getWidth() / 2), (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 350) - (startGame.getHeight() / 2));
            sb.draw(highScore, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 - (highScore.getWidth() / 2), (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 375) - (highScore.getHeight()));
            sb.draw(credits, com.mygdx.interstellarmayhem.InterstellarMayhem.WIDTH / 2 - (credits.getWidth() / 2), (com.mygdx.interstellarmayhem.InterstellarMayhem.HEIGHT - 400) - (credits.getHeight() * 1.5F));
            sb.end();
            title.stage.draw();
            player.render(sb);
        }


    public void dispose() {
        bgMusic.dispose();
        btnClick.dispose();
        player.dispose();
    }
}

