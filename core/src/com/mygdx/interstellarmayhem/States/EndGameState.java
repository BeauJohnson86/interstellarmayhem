package com.mygdx.interstellarmayhem.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.interstellarmayhem.InterstellarMayhem;
import com.mygdx.interstellarmayhem.PlayServices;
import com.mygdx.interstellarmayhem.PlayerStats;

/**
 * Created by Beau Johnson (tehBeast) on 03-Sep-16.
 */

public class EndGameState extends State {
    Sprite bg, mainMenu;
    private Music bgMusic;
    Sound btnClick, gameOver;
    PlayerStats playerStats;
    PlayServices playServices;

    //EndGameState constructor takes in a GameStateManager, String score, String time and PlayServices parameter
    public EndGameState(com.mygdx.interstellarmayhem.States.GameStateManager gsm, String score, String time, PlayServices playServices) {
        super(gsm);
        this.playServices = playServices;
        // initialize the playerStats overlay
        playerStats = new PlayerStats(score, time);
        bg = new Sprite(new Texture(Gdx.files.internal("mainMenuAssets/GalaxyBG.bmp")));
        mainMenu = new Sprite(new Texture(Gdx.files.internal("highScoreAssets/menu_button_blue.png")));
        btnClick = Gdx.audio.newSound(Gdx.files.internal("mainMenuAssets/ButtonClick.mp3"));
        gameOver = Gdx.audio.newSound(Gdx.files.internal("gameStateAssets/gameOver.ogg"));
        bgMusic = Gdx.audio.newMusic(Gdx.files.internal("mainMenuAssets/ObservingTheStar.ogg"));
        bgMusic.setLooping(true);
        bgMusic.play();
        cam.setToOrtho(false, InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT);
        super.mouse = new Vector3();
        gameOver.play();
        playServices.submitScore(Integer.parseInt(score)); // submits player score to Google Play Services
        if (Integer.parseInt(time) > 300){
            playServices.unlockAchievement4();
        }
    }


    @Override
    public void handleInput() {
        // handles touch input for the main menu button
        if (Gdx.input.justTouched()) {
            mouse.set(Gdx.input.getX(), Gdx.input.getY(), 0); //when the screen is touched, the coordinates are inserted into the vector
            cam.unproject(mouse); // calibrates the input to  camera dimension
            if (mouse.x < (InterstellarMayhem.WIDTH / 2 + (mainMenu.getWidth() / 2) - 48)
                    && mouse.x > (InterstellarMayhem.WIDTH / 2 - (mainMenu.getWidth() / 2)) + 48) {
                if (mouse.y > (InterstellarMayhem.HEIGHT - 575) - (mainMenu.getHeight() / 2)
                        && mouse.y < (InterstellarMayhem.HEIGHT - 475) - (mainMenu.getHeight() / 2)) {
                    mainMenu = new Sprite(new Texture(Gdx.files.internal("highScoreAssets/MenuBtnRed.png")));
                    btnClick.play();
                    // Timer used to allow sound from button click to play, and to conform with other
                    // sections of the game.
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            gsm.set(new com.mygdx.interstellarmayhem.States.MenuState(gsm, playServices));
                        }
                    }, InterstellarMayhem.SCREEN_DELAY);
                }
            }
        }
    }


    @Override
    public void update(float dt) {
        handleInput();
    }


    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();
        sb.draw(bg, 0,0);
        // Display menu button
        sb.draw(mainMenu, InterstellarMayhem.WIDTH/2 - (mainMenu.getWidth()/2), (InterstellarMayhem.HEIGHT - 400) - (mainMenu.getHeight() * 1.5F));
        sb.end();
        // Draws playerStats overlay on top of current screen
        playerStats.stage.draw();
    }


    @Override
    public void dispose() {
        bgMusic.dispose();
        btnClick.dispose();
    }
}

