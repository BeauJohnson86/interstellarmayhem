package com.mygdx.interstellarmayhem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Beau Johnson (tehBeast) on 03-Sep-16.
 */

public class MenuTitle implements Disposable {

    public Stage stage;
    private Viewport viewport;
    private SpriteBatch sb = new SpriteBatch();;
    protected OrthographicCamera cam;
    //Scene2D Widgets
    private Label title;


    public MenuTitle() {
        cam = new OrthographicCamera();
        cam.setToOrtho(false, InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT);
        viewport = new FillViewport(InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT, cam);
        stage = new Stage(viewport, sb);

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/timeburnerbold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 48;
        BitmapFont font = generator.generateFont(parameter); // font size 12 pixels
        generator.dispose(); // don't forget to dispose to avoid memory leaks!


        title = new Label("Interstellar Mayhem", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        //define a table used to organize hud's labels
        Table table = new Table();
        table.top();
        table.setFillParent(true);

        //add labels to table, padding the top, and giving them all equal width with expandX
        table.add(title).expandX().padTop(30);

        //add table to the stage
        stage.addActor(table);
    }


    public void update(float dt) {
    }


    @Override
    public void dispose() {
        stage.dispose();
    }
    }
