package com.mygdx.interstellarmayhem.EnvironmentObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.interstellarmayhem.InterstellarMayhem;


/**
 * Created by Beau Johnson (TehBeast) on 25-Aug-16.
 */

public class Player extends Characters {
    // private TextureAtlas playerShip;
    private final float FPS = 1 / 30f;
    private final int IMAGE_SIZE = 128;
    Animation animation1 , animation2;
    TextureAtlas playerExplode;
    float timePassed;
    boolean isImageLoop;
    int xPosition = InterstellarMayhem.WIDTH / 2;
    int yPosition = InterstellarMayhem.HEIGHT;
    private Rectangle boundsPlayer = new Rectangle();


    public Player(String name, int lives,
                  boolean isAlive, Boolean isImageLoop) {
        super.name = name;
        super.lives = lives;
        this.xPosition = xPosition - IMAGE_SIZE / 2;
        this.yPosition = yPosition - 264;
        super.isAlive = isAlive;
        this.isImageLoop = isImageLoop;
        super.ta = new TextureAtlas(Gdx.files.internal("playerAssets/bluePlayerShip.atlas"));
        playerExplode = new TextureAtlas(Gdx.files.internal("playerAssets/playerShipExplode.atlas"));
        animation1 = new Animation(FPS, ta.getRegions());
        animation2 = new Animation(FPS, playerExplode.getRegions());
        boundsPlayer = new Rectangle(getxPosition(), 125, 100, 100);
    }


    public void update(float dt) {
    }


    public void render(SpriteBatch sb) {
        sb.begin();
        timePassed += Gdx.graphics.getDeltaTime();
        sb.draw(animation1.getKeyFrame(timePassed, isImageLoop), xPosition, yPosition,
                IMAGE_SIZE, IMAGE_SIZE);
        sb.end();
    }


    public int getxPosition() {
        return this.xPosition;
    }


    public int getyPosition() {
        return this.yPosition;
    }


    public void dispose() {
        ta.dispose();
    }


    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }


    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }


    public Rectangle getBoundsPlayer() {
        return boundsPlayer;
    }


    public void playerDied() {
        final Animation tempAnimation = animation1;
        animation1 = animation2;
        Timer.schedule(new Timer.Task() {
           @Override
           public void run() {
               animation1 = tempAnimation;
           }
        }, com.mygdx.interstellarmayhem.InterstellarMayhem.DEATH_DELAY);
    }
}