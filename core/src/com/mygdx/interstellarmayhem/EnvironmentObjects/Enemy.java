package com.mygdx.interstellarmayhem.EnvironmentObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Created by Beau Johnson (tehBeast) on 28-Aug-16.
 */

public class Enemy extends Characters {

    private final float FPS = 1/30f;
    private final int IMAGE_SIZE = 128;
    Animation animation;
    float timePassed;
    boolean isImageLoop;
    int xPosition;
    int yPosition;
    private static final int FLUCTUATION = 480;
    private int randomNumber;
    private int location;
    private Vector2 enemyPosition;
    private Random rand = new Random();
    private Rectangle boundsEnemy;


    public Enemy(String name, int lives, int xPosition, int yPosition,
                  boolean isAlive, Boolean isImageLoop, TextureAtlas ta){
        super.name = name;
        super.lives = lives;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        super.isAlive = isAlive;
        this.isImageLoop = isImageLoop;
        super.ta = ta;
        animation = new Animation(FPS, ta.getRegions());
        enemyPosition = new Vector2(generateEnemyXLocation(), this.yPosition);
        boundsEnemy = new Rectangle(getEnemyPosition().x, getEnemyPosition().y, IMAGE_SIZE, IMAGE_SIZE);
    }


    public  void update(float dt){
    }


    public void render(SpriteBatch sb){
        sb.begin();
        timePassed += Gdx.graphics.getDeltaTime();
        sb.draw(animation.getKeyFrame(timePassed, isImageLoop), getEnemyPosition().x,
                getEnemyPosition().y, IMAGE_SIZE, IMAGE_SIZE);
        sb.end();
    }


    public  void dispose(){
        ta.dispose();
    }


    public Vector2 getEnemyPosition() {
        return enemyPosition;
    }


    public float generateEnemyXLocation(){
        randomNumber = rand.nextInt(FLUCTUATION);
        if (randomNumber < 352 && randomNumber > 0){
            location = randomNumber;
        }
        else if (randomNumber > 352){
            location = com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_RIGHT_BOUNDS;
        }
        else if (randomNumber < 128){
            location = com.mygdx.interstellarmayhem.InterstellarMayhem.SCREEN_LEFT_BOUNDS;
        }
        return location;
    }


    public void setEnemyYPosition(float y){
        enemyPosition.set(getEnemyPosition().x, y);
    }


    public void reposition(float y){
        setEnemyYPosition(y);
        boundsEnemy.setPosition(getEnemyPosition().x, getEnemyPosition().y);
    }


    public void setEnemyXPosition(float x){
        enemyPosition.set(x, getEnemyPosition().y);
    }


    public boolean collides(Rectangle player){
        return player.overlaps(boundsEnemy);
    }
}
