package com.mygdx.interstellarmayhem.EnvironmentObjects;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector3;

/**
 * Created by Beau Johnson (TehBeast) on 25-Aug-16.
 */

public class Characters {
    String name;
    int lives, xPosition, yPosition;
    TextureAtlas ta;
    boolean isAlive;
    protected Vector3 mouse;


    public void setLives(int lives){
        this.lives = lives;
    }


    public String getName() {
        return name;
    }


    public int getLives() {
        return lives;
    }


    public TextureAtlas getTa() {
        return ta;
    }


    public boolean isAlive() {
        return isAlive;
    }


    public int changeXDirection(int currentX, int incrementX){
        xPosition = currentX + incrementX;
        return xPosition;
    }


    public int changeYDirection(int currentY, int incrementY) {
        yPosition = currentY + incrementY;
        return yPosition;
    }
}

