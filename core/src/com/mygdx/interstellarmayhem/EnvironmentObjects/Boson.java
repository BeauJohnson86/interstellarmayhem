package com.mygdx.interstellarmayhem.EnvironmentObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.interstellarmayhem.InterstellarMayhem;
import java.util.Random;

/**
 * Created by Beau Johnson (tehBeast) on 19-Sep-16.
 */
public class Boson extends Characters {
    private static final int FLUCTUATION = 480;
    private final float FPS = 1/30f;
    private final int IMAGE_SIZE = 128;
    private boolean isImageLoop = true;
    private int randomNumber;
    private int location;
    private Vector2 bosonPosition;
    private Random rand;
    public Rectangle boundsBoson;
    private Animation animation;
    private float timePassed;


    public Boson(float y){
        this.rand = new Random();
        this.bosonPosition = new Vector2(generateBosonXLocation(), y);
        super.ta = new TextureAtlas(Gdx.files.internal("bosonAssets/bosonPulse.atlas"));
        animation = new Animation(FPS, ta.getRegions());
        boundsBoson = new Rectangle(getBosonPosition().x, getBosonPosition().y, 100, 100);
    }


    public void render(SpriteBatch sb){
        sb.begin();
        timePassed += Gdx.graphics.getDeltaTime();
        sb.draw(animation.getKeyFrame(timePassed, isImageLoop),this.bosonPosition.x, this.bosonPosition.y,
                IMAGE_SIZE, IMAGE_SIZE);
        sb.end();
    }


    public Vector2 getBosonPosition() {
        return bosonPosition;
    }


    public float generateBosonXLocation(){
        randomNumber = rand.nextInt(FLUCTUATION);
        if (randomNumber < 352 && randomNumber > 0){
            location = randomNumber;
        }
        else if (randomNumber > 352){
            location = InterstellarMayhem.SCREEN_RIGHT_BOUNDS;
        }
        else if (randomNumber < 128){
            location = InterstellarMayhem.SCREEN_LEFT_BOUNDS;
        }
        return location;
    }


    public void setBosonYPosition(float y){
        bosonPosition.set(getBosonPosition().x, y);
    }


    public void reposition(float y){
        setBosonYPosition(y);
        boundsBoson.setPosition(getBosonPosition().x, getBosonPosition().y);
    }


    public void setBosonXPosition(float x){
        bosonPosition.set(x, getBosonPosition().y);
    }


    public boolean collides(Rectangle player){
        return player.overlaps(boundsBoson);
    }


    public void dispose(){
        ta.dispose();
    }
}
        


