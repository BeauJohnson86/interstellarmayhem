package com.mygdx.interstellarmayhem.EnvironmentObjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.interstellarmayhem.InterstellarMayhem;
import java.util.Random;

/**
 * Created by Beau Johnson (tehBeast) on 25-Aug-16.
 */

public class Asteroid {
    private static final int FLUCTUATION = 480;
    private int randomNumber;
    private int location;
    private Texture asteroid;
    private Vector2 asteroidPosition;
    private Random rand;
    public Rectangle boundsAsteroid;


    public Asteroid(float y){
        this.asteroid = new Texture("gameStateAssets/asteroid.png");
        this.rand = new Random();
        this.asteroidPosition = new Vector2(generateAsteroidXLocation(), y);
        boundsAsteroid = new Rectangle(getAsteroidPosition().x, getAsteroidPosition().y, 100, 100);
        System.out.println("Asteroid Rect: " + boundsAsteroid.toString());
    }


    public Texture getAsteroid() {
        return asteroid;
    }


    public Vector2 getAsteroidPosition() {
        return asteroidPosition;
    }


    public float generateAsteroidXLocation(){
        randomNumber = rand.nextInt(FLUCTUATION);
        if (randomNumber < 352 && randomNumber > 0){
            location = randomNumber;
        }
        else if (randomNumber > 352){
            location = InterstellarMayhem.SCREEN_RIGHT_BOUNDS;
        }
        else if (randomNumber < 128){
            location = InterstellarMayhem.SCREEN_LEFT_BOUNDS;
        }
        return location;
    }


    public void setAsteroidYPosition(float y){
        asteroidPosition.set(getAsteroidPosition().x, y);
    }


    public void reposition(float y){
        setAsteroidYPosition(y);
        boundsAsteroid.setPosition(getAsteroidPosition().x, getAsteroidPosition().y);
    }


    public void setAsteroidXPosition(float x){
        asteroidPosition.set(x, getAsteroidPosition().y);
    }


    public boolean collides(Rectangle player){
        return player.overlaps(boundsAsteroid);
    }


    public void dispose(){
        asteroid.dispose();
    }
}

