package com.mygdx.interstellarmayhem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Beau Johnson (tehBeast) on 24-Sep-16.
 */

public class Credits {


    public Stage stage;
    private Viewport viewport;
    //score && gameAuthor tracking variables
    private Integer worldTimer;
    private int score;
    private SpriteBatch sb = new SpriteBatch();;
    protected OrthographicCamera cam;

    //Scene2D Widgets
    private Label MusicLabel, spritesLabel,gameLabel, screenLabel, thanksLabel ;

    private Label spritesAuthor, musicAuthor, gameAuthor;


    public Credits() {
        cam = new OrthographicCamera();
        cam.setToOrtho(false, InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT);
        //define tracking variables
        worldTimer = 0;
        score = 0;
        //setup the HUD viewport using a new camera seperate from gamecam
        //define stage using that viewport and games spritebatch
        viewport = new FillViewport(InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT, cam);
        stage = new Stage(viewport, sb);

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/timeburnerbold.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 18;
        BitmapFont font = generator.generateFont(parameter);
        parameter.size = 48;// font size 12 pixels
        BitmapFont heading = generator.generateFont(parameter);
        parameter.size = 28;// font size 12 pixels
        BitmapFont subHeading = generator.generateFont(parameter);
        generator.dispose(); // don't forget to dispose to avoid memory leaks!

        //define labels using the String, and a Label style consisting of a font and color
        screenLabel = new Label("Credits", new Label.LabelStyle(heading, Color.valueOf("09fcfd")));
        spritesLabel = new Label("Sprites and Animations: ", new Label.LabelStyle(subHeading, Color.valueOf("09fcfd")));
        MusicLabel = new Label("Music and Sound Effects: ", new Label.LabelStyle(subHeading, Color.valueOf("09fcfd")));
        gameLabel = new Label("Game Created By: ", new Label.LabelStyle(subHeading, Color.valueOf("09fcfd")));
        thanksLabel = new Label("I would like to thank the people listed below\n for their contributions to this game through\n" +
                                "                the use of their free assets.", new Label.LabelStyle(font, Color.valueOf("09fcfd")));

        spritesAuthor = new Label("Space Odyssey Pack from opengameart.org\n created by UnLucky Studio - Author: sujit1717", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        musicAuthor = new Label("ObservingTheStar from opengameart.org - Author: yd", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        gameAuthor = new Label("Beau Johnson - tehBeast", new Label.LabelStyle(font, Color.valueOf("09fcfd")));

        //define a table used to organize hud's labels
        Table table = new Table();
        table.top();
        //table.setDebug(true);
        table.setFillParent(true);

        //add labels to table, padding the top, and giving them all equal width with expandX
        table.row().padTop(50);
        table.add(screenLabel).expandX();
        table.row().padTop(25);
        table.add(thanksLabel).expandX();
        table.row().padTop(25);
        table.add(MusicLabel).expandX();
        table.row().padTop(15);
        table.add(musicAuthor).expandX();
        table.row().padTop(25);
        table.add(spritesLabel).expandX();
        table.row().padTop(15);
        table.add(spritesAuthor).expandX();
        table.row().padTop(25);
        table.add(gameLabel).expandX();
        table.row().padTop(15);
        table.add(gameAuthor).expandX();

        //add table to the stage
        stage.addActor(table);
    }


    public void update(float dt) {
    }


    public void dispose() {
        stage.dispose();
        sb.dispose();
    }
}
