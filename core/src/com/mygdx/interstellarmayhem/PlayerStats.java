package com.mygdx.interstellarmayhem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Beau Johnson (tehBeast) on 24-Sep-16.
 */


    public class PlayerStats implements Disposable {

        public Stage stage;
        private Viewport viewport;
        //score && time tracking variables
        private Integer worldTimer;
        private String score;
        private String time;
        private String bosons;
        private SpriteBatch sb = new SpriteBatch();;
        protected OrthographicCamera cam;

        //Scene2D Widgets
        private Label bosonsCollectedLabel, playerScoreLabel,timeLabel, screenLabel ;
        private Label numberBosons, playerScore, playerTime;


        public PlayerStats(String score, String time) {
            cam = new OrthographicCamera();
            cam.setToOrtho(false, InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT);
            //define tracking variables
            worldTimer = 0;
            this.score = score;
            this.time = time;
            //setup the HUD viewport using a new camera seperate from gamecam
            //define stage using that viewport and games spritebatch
            viewport = new FillViewport(InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT, cam);
            stage = new Stage(viewport, sb);
            bosons = String.valueOf(Integer.parseInt(this.score) / 100); //calculates bosons collected

            // Generate new Bitmap font so that I can use this in the game without it being blury
            FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/timeburnerbold.ttf"));
            FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            parameter.size = 18;
            BitmapFont font = generator.generateFont(parameter);
            parameter.size = 48;
            BitmapFont heading = generator.generateFont(parameter);
            generator.dispose(); // Dispose of generator to avoid memory leaks!

            //define labels using the String, and a Label style consisting of a font and color
            screenLabel = new Label("GAME OVER", new Label.LabelStyle(heading, Color.valueOf("09fcfd")));
            playerScoreLabel = new Label("YOUR SCORE: ", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
            bosonsCollectedLabel = new Label("BOSON PULSES COLLECTED: ", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
            timeLabel = new Label("TIME: ", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
            numberBosons = new Label(bosons, new Label.LabelStyle(font, Color.valueOf("09fcfd")));
            playerScore = new Label(score, new Label.LabelStyle(font, Color.valueOf("09fcfd")));
            playerTime = new Label(time, new Label.LabelStyle(font, Color.valueOf("09fcfd")));

            //define a table used to organize hud's labels
            Table table = new Table();
            table.top();
            table.setFillParent(true);

            //add labels to table, padding the top, and giving them all equal width with expandX
            table.row().padTop(100);
            table.add(screenLabel).expandX();
            table.row().padTop(75);
            table.add(bosonsCollectedLabel).expandX();
            table.row().padTop(10);
            table.add(numberBosons).expandX();
            table.row().padTop(50);
            table.add(timeLabel).expandX();
            table.row().padTop(10);
            table.add(playerTime).expandX();
            table.row().padTop(50);
            table.add(playerScoreLabel).expandX();
            table.row().padTop(10);
            table.add(playerScore).expandX();

            //add table to the stage
            stage.addActor(table);
        }


        public void update(float dt) {
        }


        @Override
        public void dispose() {
            stage.dispose();
            sb.dispose();
        }
    }

