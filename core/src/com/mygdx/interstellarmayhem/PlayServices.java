package com.mygdx.interstellarmayhem;

/**
 * Created by Beau Johnson (tehBeast) on 26-Sep-16.
 */

public interface PlayServices
{
    public void signIn();
    public void submitScore(int highScore);
    public void showScore();
    public boolean isSignedIn();
    public void unlockAchievement1();
    public void unlockAchievement2();
    public void unlockAchievement3();
    public void unlockAchievement4();
    public void showMsg1();
    public void stopMsg1();
    public void showMsg2();
    public void stopMsg2();
}