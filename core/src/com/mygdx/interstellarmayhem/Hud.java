package com.mygdx.interstellarmayhem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by Beau Johnson (tehBeast) on 03-Sep-16.
 */

public class Hud implements Disposable {

    public Stage stage;
    private Viewport viewport;
    //score && time tracking variables
    private Integer worldTimer;
    private int score;
    private int lives;
    private SpriteBatch sb = new SpriteBatch();;
    protected OrthographicCamera cam;
    //Scene2D Widgets
    private Label timeCountLabel, lifeCountLabel, timeLabel, scoreLabel, lifeLabel;
    private static Label scoreCountLabel;


    public Hud() {
        cam = new OrthographicCamera();
        cam.setToOrtho(false, InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT);
        //define tracking variables
        lives = 3;
        worldTimer = 0;
        score = 0;
        //setup the HUD viewport using a new camera seperate from gamecam
        //define stage using that viewport and games spritebatch
        viewport = new FillViewport(InterstellarMayhem.WIDTH, InterstellarMayhem.HEIGHT, cam);
        stage = new Stage(viewport, sb);

        // Generate new Bitmap font so that I can use this in the game without it being blury
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/timeburnernormal.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 18;
        BitmapFont font = generator.generateFont(parameter);
        generator.dispose(); // dispose of generator to avoid memory leaks!

        //define labels using the String, and a Label style consisting of a font and color
        timeCountLabel = new Label(String.format("%03d", worldTimer), new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        lifeCountLabel = new Label(String.format("%01d", worldTimer), new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        scoreCountLabel = new Label(String.format("%01d", score), new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        timeLabel = new Label("TIME", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        lifeLabel = new Label("LIVES", new Label.LabelStyle(font, Color.valueOf("09fcfd")));
        scoreLabel = new Label("SCORE", new Label.LabelStyle(font, Color.valueOf("09fcfd")));

        //define a table used to organize hud's labels
        Table table = new Table();
        table.top();
        table.setFillParent(true);

        //add labels to table, padding the top, and giving them all equal width with expandX
        table.add(scoreLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.add(lifeLabel).expandX().padTop(10);
        table.row();
        table.add(scoreCountLabel).expandX();
        table.add(timeCountLabel).expandX();
        table.add(lifeCountLabel).expandX();

        //add table to the stage
        stage.addActor(table);
    }


    public void update(float dt) {
        // generates the time and updates its label on the hud
        worldTimer++;
        if (worldTimer % 60 == 0) {
            timeCountLabel.setText(String.format("%01d", (worldTimer/60)));
        }
    }


    public void addScore(int value) {
        // updates the score when this method is called and updates the label
        score = value;
        scoreCountLabel.setText(String.format("%01d", score));
    }


    public void updateLives(int value){
        // updates the lives when this method is called and updates the label
        this.lives = value;
        lifeCountLabel.setText(String.format("%01d", this.lives));
    }


    @Override
    public void dispose() {
        stage.dispose();
    }

    public String getTime() {
        return String.valueOf(worldTimer/60);
    }

    public int getScore() {
        return score;
    }
}